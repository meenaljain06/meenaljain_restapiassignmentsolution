package com.great.learning.ems.controller;

import com.great.learning.ems.model.Role;
import com.great.learning.ems.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping("/roles")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @PostMapping("/save")
    public Role save(@RequestParam("name") String name) {
        Role role = new Role(name);
        if (!roleService.existsById(role.getId())) {
            try {
                roleService.save(role);
                return role;
            } catch (DataIntegrityViolationException e) {
                throw new ResponseStatusException(HttpStatus.CONFLICT,
                        "Role with name - " + role.getName() + " already exists");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.CONFLICT,
                    "Role with id - " + role.getId() + " already exists");
        }
    }

    @GetMapping("/findAll")
    public List<Role> findAll() {
        return roleService.findAll();
    }

    @GetMapping("/findById")
    public Optional<Role> findById(@RequestParam("roleId") int id) {
        try {
            return roleService.findById(id);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No role found with id - " + id);
        }
    }

    @PutMapping("/updateById")
    public Role updateById(@RequestBody Role role) {
        if (roleService.existsById(role.getId())) {
            try {
                roleService.save(role);
                return role;
            } catch (DataIntegrityViolationException e) {
                throw new ResponseStatusException(HttpStatus.CONFLICT,
                        "Role with name - " + role.getName() + " already exists");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No role found with id - " + role.getId());
        }
    }

    @DeleteMapping("/deleteById")
    public String deleteById(@RequestParam("roleId") int id) {
        try {
            roleService.deleteById(id);
            return "Deleted role id - " + id;
        } catch (EmptyResultDataAccessException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No role found with id - " + id);
        }
    }
}
