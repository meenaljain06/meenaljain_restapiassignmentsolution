package com.great.learning.ems.service;

import com.great.learning.ems.model.Employee;
import com.great.learning.ems.model.Role;
import com.great.learning.ems.model.User;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {
    public boolean existsById(int id);
    public List<Employee> findAll();

    public Optional<Employee> findById(int theId);

    public String save(Employee theEmployee);

    public String updateEmployee(Employee theEmployee);

    public String deleteById(int theId);

    public List<Employee> searchByFirstName(String firstName);

    public List<Employee> sortByFirstName(String sortBy);

 }
