package com.great.learning.ems.controller;

import com.great.learning.ems.model.User;
import com.great.learning.ems.service.RoleService;
import com.great.learning.ems.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @PostMapping("/save")
    public User save(@RequestBody User user) {
        if (!roleService.existsById(user.getId())) {
           if (!userService.existsById(user.getId())) {
                try {
                    userService.save(user);
                    return user;
                } catch (DataIntegrityViolationException e) {
                    throw new ResponseStatusException(HttpStatus.CONFLICT,
                            "User with username - " + user.getUsername() + " already exists");
                }
            } else {
                throw new ResponseStatusException(HttpStatus.CONFLICT,
                        "User with id - " + user.getId() + " already exists");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.FAILED_DEPENDENCY,
                    "No role(s) found with name(s) - " + Arrays.toString(new List[]{user.getRoles()}));
        }
    }

    @GetMapping("/findAll")
    public List<User> findAll() {
        return userService.findAll();
    }

    @GetMapping("/findById")
    public Optional<User> findById(@RequestParam("userId") int id) {
        try {
            return userService.findById(id);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No user found with id - " + id);
        }
    }

    @DeleteMapping("/deleteById")
    public String deleteById(@RequestParam("userId") int id) {
        try {
            userService.deleteById(id);
            return "Deleted user id - " + id;
        } catch (EmptyResultDataAccessException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No user found with id - " + id);
        }
    }

}
