package com.great.learning.ems.repository;

import com.great.learning.ems.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    List<Employee> findByFirstNameContainsAllIgnoreCase(String firstName);

    }
