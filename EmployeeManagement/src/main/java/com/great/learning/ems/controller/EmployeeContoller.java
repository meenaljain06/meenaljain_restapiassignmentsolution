package com.great.learning.ems.controller;

import com.great.learning.ems.model.Employee;
import com.great.learning.ems.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping("/employee")
public class EmployeeContoller {
    @Autowired
    private EmployeeService employeeService;

    @PostMapping("/save")
    public Employee save(@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName,
                         @RequestParam("email") String email) {
        Employee employee = new Employee(firstName, lastName, email);
        if (!employeeService.existsById(employee.getId())) {
            employeeService.save(employee);
            return employee;
        } else {
            throw new ResponseStatusException(HttpStatus.CONFLICT,
                    "Employee with id - " + employee.getId() + " already exists");
        }
    }

    @GetMapping("/findAll")
    public List<Employee> findAll() {
        return employeeService.findAll();
    }

    @GetMapping("/findById")
    public Optional<Employee> findById(@RequestParam("employeeId") int id) {
        try {
            return employeeService.findById(id);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No employee found with id - " + id);
        }
    }

    @PutMapping("/updateById")
    public Employee updateById(@RequestBody Employee employee) {
        if (employeeService.existsById(employee.getId())) {
            employeeService.save(employee);
            return employee;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No employee found with id - " + employee.getId());
        }
    }

    @DeleteMapping("/deleteById")
    public String deleteById(@RequestParam("employeeId") int id) {
        try {
            employeeService.deleteById(id);
            return "Deleted employee id - " + id;
        } catch (EmptyResultDataAccessException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No employee found with id - " + id);
        }
    }

    @GetMapping("/findByFirstNameContainsAllIgnoreCase")
    public List<Employee> findByFirstNameContainsAllIgnoreCase(@RequestParam("firstName") String firstName) {
        return employeeService.searchByFirstName(firstName);
    }

    @GetMapping("/sortByFirstNameDirection")
    public List<Employee> sortByFirstNameDirection(@RequestParam String sortBy) {
        return employeeService.sortByFirstName(sortBy);
    }
}
