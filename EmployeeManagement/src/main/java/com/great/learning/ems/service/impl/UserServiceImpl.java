package com.great.learning.ems.service.impl;

import com.great.learning.ems.model.User;
import com.great.learning.ems.repository.UserRepository;
import com.great.learning.ems.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;

    @Override
    public boolean existsById(int id) {
        return userRepository.existsById(id);
    }

    @Override
    @Transactional
    public List<User> findAll() {
        List<User> users = userRepository.findAll();
        return users;
    }

    @Override
    @Transactional
    public Optional<User> findById(Integer id) {
        return userRepository.findById(id);
    }

    @Override
    @Transactional
    public void save(User theUser) {
        userRepository.save(theUser);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        userRepository.deleteById(id);
    }

    @Override
    @Transactional
    public Optional<User> findByUserName(String userName) {
        return Optional.ofNullable(userRepository.getUserByUsername(userName));
    }
}
