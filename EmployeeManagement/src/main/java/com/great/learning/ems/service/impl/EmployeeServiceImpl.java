package com.great.learning.ems.service.impl;

import com.great.learning.ems.model.Employee;
import com.great.learning.ems.model.Role;
import com.great.learning.ems.model.User;
import com.great.learning.ems.repository.EmployeeRepository;
import com.great.learning.ems.repository.RoleRepository;
import com.great.learning.ems.repository.UserRepository;
import com.great.learning.ems.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService{
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    BCryptPasswordEncoder bcryptEncoder;

    @Override
    public boolean existsById(int id) {
        return employeeRepository.existsById(id);
    }

    @Override
    public List<Employee> findAll() {

        return employeeRepository.findAll();
    }

    @Override
    public Optional<Employee> findById(int id) {
        boolean ifEmployeeExist = employeeRepository.existsById(id);
        if (ifEmployeeExist) {
            return employeeRepository.findById(id);

        } else
            throw new RuntimeException("There is no Employee with id : " + id);

    }

    @Override
    public String save(Employee employee) {

        if (employee.getFirstName().equals("") || employee.getLastName().equals("")
                || employee.getEmail().equals("")) {
            throw new RuntimeException("Error!!! All fields are mandatory ");

        } else {
            employeeRepository.saveAndFlush(employee);
            return "Below Employee Details Added Successfully \nid : " + employee.getId()
                    + "\nFirst Name : " + employee.getFirstName() + "\nLast Name : " + employee.getLastName()
                    + "\nEmail : " + employee.getEmail();
        }
    }

    @Override
    public String updateEmployee(Employee employee) {
        boolean ifEmployeeExist = employeeRepository.existsById(employee.getId());

        if (ifEmployeeExist) {
            employeeRepository.saveAndFlush(employee);
            return "Below Employee Details Updated Successfully \nid : " + employee.getId()
                    + "\nFirst Name : " + employee.getFirstName() + "\nLast Name : " + employee.getLastName()
                    + "\nEmail : " + employee.getEmail();
        } else {
            return "There is no Employee with id : " + employee.getId();

        }

    }

    @Override
    public String deleteById(int theId) {

        boolean ifEmployeeExist = employeeRepository.existsById(theId);

        if (ifEmployeeExist) {
            employeeRepository.deleteById(theId);
            return "Deleted employee id -" + theId;
        } else
            return "There is no Employee with id : " + theId;
    }

    @Override
    public List<Employee> searchByFirstName(String firstName) {
        List<Employee> employees = employeeRepository.findByFirstNameContainsAllIgnoreCase(firstName);
        if (employees.size() > 0)
            return employees;
        else
            throw new RuntimeException("Employee Details not found!!!");
    }

    @Override
    public List<Employee> sortByFirstName(String sortBy) {

        List<Employee> employees = employeeRepository.findAll(Sort.by(Sort.Direction.fromString(sortBy), "firstName"));
        if (employees.size() > 0)

            return employees;
        else
            throw new RuntimeException("Employee Details not found!!!");
    }


}
