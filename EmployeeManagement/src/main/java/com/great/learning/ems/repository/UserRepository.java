package com.great.learning.ems.repository;

import com.great.learning.ems.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, Integer> {
    @Query(value = "SELECT * FROM User u WHERE u.username = ?1", nativeQuery = true)
    public User getUserByUsername(String username);
}
