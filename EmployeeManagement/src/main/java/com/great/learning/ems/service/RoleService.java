package com.great.learning.ems.service;

import com.great.learning.ems.model.Role;

import java.util.List;
import java.util.Optional;

public interface RoleService {
    public boolean existsById(int id);
    public List<Role> findAll();

    public Optional<Role> findById(int theId);

    public void save(Role theRole);

    public void deleteById(int theId);

    public Role findByName(String name);
}
