package com.great.learning.ems.controller;

import com.great.learning.ems.model.Employee;
import com.great.learning.ems.model.User;
import com.great.learning.ems.service.EmployeeService;
import com.great.learning.ems.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;

import java.security.Principal;
import java.util.List;

@Controller
@ApiIgnore
public class HomeController {

    @Autowired
    private UserService userService;

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping("/")
    public String list(Model model, Principal principal) {
        List<User> users = userService.findAll();
        model.addAttribute("Users", users);
        List<Employee> employees = employeeService.findAll();
        model.addAttribute("Employees", employees);
        model.addAttribute("LoggedInUser", principal != null ? principal.getName() : null);
        return "index";
    }
}
