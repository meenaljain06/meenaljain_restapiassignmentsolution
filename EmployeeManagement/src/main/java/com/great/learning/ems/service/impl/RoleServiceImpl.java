package com.great.learning.ems.service.impl;

import com.great.learning.ems.model.Role;
import com.great.learning.ems.repository.RoleRepository;
import com.great.learning.ems.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    RoleRepository roleRepository;

    @Override
    public boolean existsById(int id) {
        return roleRepository.existsById(id);
    }

    @Override
    @Transactional
    public List<Role> findAll() {
        List<Role> roles = roleRepository.findAll();
        return roles;
    }

    @Override
    @Transactional
    public Optional<Role> findById(int id) {
        return roleRepository.findById(id);
    }

    @Override
    @Transactional
    public void save(Role theRole) {
        roleRepository.save(theRole);
    }

    @Override
    @Transactional
    public void deleteById(int theId) {
        roleRepository.deleteById(theId);
    }

    @Override
    public Role findByName(String name) {
          return roleRepository.getRoleByName(name);
    }

}
