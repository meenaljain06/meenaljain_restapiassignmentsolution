package com.great.learning.ems.service;

import com.great.learning.ems.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    public boolean existsById(int id);

    public List<User> findAll();

    public Optional<User> findById(Integer theId);

    public void save(User theUser);

    public void deleteById(Integer theId);

    public Optional<User> findByUserName(String userName);
}
